"use strict"

const imagesWrapper = [...document.getElementById("images-wrapper").children];
let index = 0;
const stopBtn = document.getElementById("stop-button");
const resumeBtn = document.getElementById("resume-button");

let timer = setInterval(imageToShow, 3000);
function imageToShow() {
    imagesWrapper[index].classList.remove("image-to-show");
    index = (index + 1) % imagesWrapper.length;
    console.log(index);
    imagesWrapper[index].classList.add("image-to-show");

    resumeBtn.disabled = true;
}

stopBtn.addEventListener("click", () => {
    clearInterval(timer);
    stopBtn.disabled = true;
    resumeBtn.disabled = false;
})

resumeBtn.addEventListener("click", () => {
    timer = setInterval(imageToShow, 3000);
    resumeBtn.disabled = true;
    stopBtn.disabled = false;
})

