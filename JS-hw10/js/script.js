'use strict'

const passwordEnter = document.getElementById("input-password-enter");
const passwordConfirm = document.getElementById("input-password-confirm");
const confirmLabel = document.getElementById("confirm-label");
const submitButton = document.getElementById("submit-button");
const warningSpan = document.createElement("span");

warningSpan.className = "password-confirm";
warningSpan.innerText = "Нужно ввести одинаковые значения";

submitButton.addEventListener("click", ev => {
    ev.preventDefault();
    if (passwordEnter.value !== passwordConfirm.value || passwordEnter.value === "") {
        confirmLabel.append(warningSpan);
    } else {
        alert("You are welcome");
    }
})

passwordConfirm.onfocus = function () {
    warningSpan.remove();
}

passwordEnter.onfocus = function () {
    warningSpan.remove();
}

const showPasswordIcon = document.getElementById("fa-eye");
const hidePasswordIcon = document.getElementById("fa-eye-slash");

function faEye(icon, input) {
    if (icon.classList.contains("fa-eye")) {
        input.setAttribute("type", "text");
        icon.classList.remove("fa-eye");
        icon.classList.add("fa-eye-slash");
    } else {
        input.setAttribute("type", "password");
        icon.classList.remove("fa-eye-slash");
        icon.classList.add("fa-eye");
    }
}

showPasswordIcon.addEventListener("click", () => faEye(showPasswordIcon,passwordEnter));
hidePasswordIcon.addEventListener("click", () => faEye(hidePasswordIcon,passwordConfirm));
