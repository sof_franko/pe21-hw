const amazingWorkArr = [
    {
        img: "./images/amazing-work/amazing-graphic-photo/category-graphic-1.jpg",
        category: "Graphic Design"
    },
    {
        img: "./images/amazing-work/amazing-web-photo/category-web-1.jpg",
        category: "Web Design"
    },
    {
        img: "./images/amazing-work/amazing-landing-photo/category-landing-1.jpg",
        category: "Landing Pages"
    },
    {
        img: "./images/amazing-work/amazing-wordpress-photo/category-wordpress-1.jpg",
        category: "Wordpress"
    },
    {
        img: "./images/amazing-work/amazing-graphic-photo/category-graphic-2.jpg",
        category: "Graphic Design"
    },
    {
        img: "./images/amazing-work/amazing-web-photo/category-web-2.png",
        category: "Web Design"
    },
    {
        img: "./images/amazing-work/amazing-landing-photo/category-landing-2.jpg",
        category: "Landing Pages"
    },
    {
        img: "./images/amazing-work/amazing-wordpress-photo/category-wordpress-2.jpg",
        category: "Wordpress"
    },
    {
        img: "./images/amazing-work/amazing-graphic-photo/category-graphic-3.jpg",
        category: "Graphic Design"
    },
    {
        img: "./images/amazing-work/amazing-web-photo/category-web-3.png",
        category: "Web Design"
    },
    {
        img: "./images/amazing-work/amazing-landing-photo/category-landing-3.jpg",
        category: "Landing Pages"
    },
    {
        img: "./images/amazing-work/amazing-wordpress-photo/category-wordpress-3.jpg",
        category: "Wordpress"
    },
    {
        img: "./images/amazing-work/amazing-graphic-photo/category-graphic-4.jpg",
        category: "Graphic Design"
    },
    {
        img: "./images/amazing-work/amazing-web-photo/category-web-4.png",
        category: "Web Design"
    },
    {
        img: "./images/amazing-work/amazing-landing-photo/category-landing-4.jpg",
        category: "Landing Pages"
    },
    {
        img: "./images/amazing-work/amazing-wordpress-photo/category-wordpress-4.jpg",
        category: "Wordpress"
    },
    {
        img: "./images/amazing-work/amazing-graphic-photo/category-graphic-5.jpg",
        category: "Graphic Design"
    },
    {
        img: "./images/amazing-work/amazing-web-photo/category-web-5.png",
        category: "Web Design"
    },
    {
        img: "./images/amazing-work/amazing-landing-photo/category-landing-5.jpg",
        category: "Landing Pages"
    },
    {
        img: "./images/amazing-work/amazing-wordpress-photo/category-wordpress-5.jpg",
        category: "Wordpress"
    },
    {
        img: "./images/amazing-work/amazing-graphic-photo/category-graphic-6.jpg",
        category: "Graphic Design"
    },
    {
        img: "./images/amazing-work/amazing-web-photo/category-web-6.png",
        category: "Web Design"
    },
    {
        img: "./images/amazing-work/amazing-landing-photo/category-landing-6.jpg",
        category: "Landing Pages"
    },
    {
        img: "./images/amazing-work/amazing-wordpress-photo/category-wordpress-6.jpg",
        category: "Wordpress"
    },
    {
        img: "./images/amazing-work/amazing-graphic-photo/category-graphic-7.jpg",
        category: "Graphic Design"
    },
    {
        img: "./images/amazing-work/amazing-web-photo/category-web-7.png",
        category: "Web Design"
    },
    {
        img: "./images/amazing-work/amazing-landing-photo/category-landing-7.jpg",
        category: "Landing Pages"
    },
    {
        img: "./images/amazing-work/amazing-wordpress-photo/category-wordpress-7.jpg",
        category: "Wordpress"
    },
    {
        img: "./images/amazing-work/amazing-graphic-photo/category-graphic-8.jpg",
        category: "Graphic Design"
    },
    {
        img: "./images/amazing-work/amazing-web-photo/category-web-8.png",
        category: "Web Design"
    },
    {
        img: "./images/amazing-work/amazing-landing-photo/category-landing-8.jpg",
        category: "Landing Pages"
    },
    {
        img: "./images/amazing-work/amazing-wordpress-photo/category-wordpress-8.jpg",
        category: "Wordpress"
    },
    {
        img: "./images/amazing-work/amazing-graphic-photo/category-graphic-9.jpg",
        category: "Graphic Design"
    },
    {
        img: "./images/amazing-work/amazing-web-photo/category-web-9.png",
        category: "Web Design"
    },
    {
        img: "./images/amazing-work/amazing-landing-photo/category-landing-9.jpg",
        category: "Landing Pages"
    },
    {
        img: "./images/amazing-work/amazing-wordpress-photo/category-wordpress-9.jpg",
        category: "Wordpress"
    },
    {
        img: "./images/amazing-work/amazing-graphic-photo/category-graphic-10.jpg",
        category: "Graphic Design"
    },
    {
        img: "./images/amazing-work/amazing-web-photo/category-web-10.png",
        category: "Web Design"
    },
    {
        img: "./images/amazing-work/amazing-landing-photo/category-landing-10.jpg",
        category: "Landing Pages"
    },
    {
        img: "./images/amazing-work/amazing-wordpress-photo/category-wordpress-10.jpg",
        category: "Wordpress"
    },
    {
        img: "./images/amazing-work/amazing-graphic-photo/category-graphic-11.jpg",
        category: "Graphic Design"
    },
    {
        img: "./images/amazing-work/amazing-web-photo/category-web-11.png",
        category: "Web Design"
    },
    {
        img: "./images/amazing-work/amazing-landing-photo/category-landing-11.jpg",
        category: "Landing Pages"
    },
    {
        img: "./images/amazing-work/amazing-wordpress-photo/category-wordpress-11.jpg",
        category: "Wordpress"
    },
    {
        img: "./images/amazing-work/amazing-graphic-photo/category-graphic-12.jpg",
        category: "Graphic Design"
    },
    {
        img: "./images/amazing-work/amazing-web-photo/category-web-12.png",
        category: "Web Design"
    },
    {
        img: "./images/amazing-work/amazing-landing-photo/category-landing-12.jpg",
        category: "Landing Pages"
    },
    {
        img: "./images/amazing-work/amazing-wordpress-photo/category-wordpress-12.jpg",
        category: "Wordpress"
    }
]

const amazingWorkTitlesList = document.getElementById(
    "services-titles-list");
const amazingWorkContentList = document.getElementById("work-content-list");
const loadBtn = document.getElementById("load-work-btn");
let length = 0;


const filteredWorkArr = [].concat(amazingWorkArr).filter((item, index) => index < 12)


function renderAll(array) {
    array.map(showCategoryPhoto)
}
renderAll(filteredWorkArr);

amazingWorkTitlesList.addEventListener("click", showCategories);

loadBtn.addEventListener("click", () => {
    const renderAllImg = [].concat(amazingWorkArr)

    if (length === 0 || length === 36) {
        renderAll(renderAllImg.splice(12, 12))
        length = 12
    } else if (length === 12) {
        renderAll(renderAllImg.splice(24, 12))
        length = 24
    } else if (length === 24) {
        renderAll(renderAllImg.splice(36, 12))
        length = 36
        loadBtn.remove()
    }
})

 function showCategories(event) {

        document.querySelector(".work-active-title-tab").classList.remove("work-active-title-tab");
        event.target.classList.add("work-active-title-tab");

        amazingWorkContentList.innerHTML = "";

        if (event.target.innerHTML === "All") {
            renderAll(filteredWorkArr);
            document.querySelector(".amazing-work-list-container").append(loadBtn)
        } else {
            amazingWorkArr.filter(item => item.category === event.target.innerHTML).map(showCategoryPhoto)
            length = 0
            loadBtn.remove()
        }

}

function showCategoryPhoto (item) {
    amazingWorkContentList.insertAdjacentHTML("beforeend", `
        <li>
        <div class="amazing-work-photo-container">
            <div class="amazing-work-photo">
                <img src="${item.img}" alt="amazing-work-photo" class="amazing-work-photo-img">
            </div>
            
            <div class="hidden-hover">
                <div class="amazing-work-photo-hover amazing-work-photo-img">
                <div class="amazing-work-hover-icons-container">
                    <a href="#">
                        <i class="fas fa-link work-photo-hover-icon turquoise-color"></i> 
                    </a><a href="#">
                        <i class="fas fa-square work-photo-hover-icon turquoise-color"></i>
                    </a>
                </div>
                    <p class="amazing-work-photo-hover-title turquoise-color">creative design</p>
                    <a class="amazing-work-photo-hover-description">${item.category}</a>
                </div>
            
</div>
        </div>
    </li>
        `)
}

