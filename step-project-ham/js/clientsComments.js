const currentContent = document.querySelectorAll(".clients-content-item");
const currentCarouselImg = document.querySelectorAll(".clients-carousel-item");

let currentItem = 0;

function showContentItem(targetItem) {
    currentCarouselImg[currentItem].classList.remove("active-carousel-item");
    currentContent[currentItem].classList.add("hidden-content-item");

    currentCarouselImg[targetItem].classList.add("active-carousel-item");
    currentContent[targetItem].classList.remove("hidden-content-item");

    currentItem = targetItem;
}

currentCarouselImg.forEach((item, index) => {
        item.addEventListener("click", ev => {
            showContentItem(index)
        })
    })


const previousBtn = document.getElementById("previous-btn");
const nextBtn = document.getElementById("next-btn");

let targetItem = 0;

previousBtn.addEventListener("click", previousBtnClick);
nextBtn.addEventListener("click", nextBtnClick);

function previousBtnClick() {
    if (currentItem === 0) {
        targetItem = currentContent.length - 1;
    } else {
        targetItem = currentItem - 1;
    }
    showContentItem(targetItem)
}

function nextBtnClick() {
    if (currentItem === currentContent.length - 1) {
        targetItem = 0;
    } else {
        targetItem = currentItem + 1;
    }
    showContentItem(targetItem)
}

