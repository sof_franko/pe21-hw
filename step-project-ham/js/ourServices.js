const servicesTabsSwitch = document.getElementById("servicesTitlesList");
const servicesTabsList = [...document.getElementById("servicesTitlesList").children];
const servicesTabsContentList = [...document.getElementById("servicesTabsContentList").children];
const servicesFirstChildContent = document.getElementById("servicesTabsContentList").firstElementChild;

// if (servicesTabsList.length !== servicesTabsContentList.length) {
//     console.error("Something went wrong. Please check an amount of items in each list")
// }

servicesTabsSwitch.addEventListener("click", ev => {
    const servicesTabIndex = servicesTabsList.indexOf(ev.target);

    servicesTabsList.forEach(tab => tab.classList.remove("services-active-title-tab"));
    servicesTabsList[servicesTabIndex].classList.add("services-active-title-tab");

    servicesTabsContentList.forEach(tabContent => tabContent.hidden = true);
    servicesTabsContentList[servicesTabIndex].hidden = false;

})

function showFirstTab() {
    servicesTabsContentList.forEach(tabContent => tabContent.hidden = true);
    servicesFirstChildContent.hidden = false;
}

showFirstTab();