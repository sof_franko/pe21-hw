'use strict'

const tabsSwitch = document.getElementById("tabsList");
const tabsList = [...document.getElementById("tabsList").children];
const tabsContentList = [...document.getElementById("tabsContentList").children];
const firstChildContent = document.getElementById("tabsContentList").firstElementChild;

if (tabsList.length !== tabsContentList.length) {
    console.error("Something went wrong. Please check an amount of items in each list")
}

window.onload = () => {
    tabsContentList.forEach(tabContent => tabContent.hidden = true);
    firstChildContent.hidden = false;
}

tabsSwitch.addEventListener("click", ev => {
    const tabIndex = tabsList.indexOf(ev.target);

    tabsList.forEach(tab => tab.classList.remove("active"));
    tabsList[tabIndex].classList.add("active");

    tabsContentList.forEach(tabContent => tabContent.hidden = true);
    tabsContentList[tabIndex].hidden = false;

})