//  Описать своими словами для чего вообще нужны функции в программировании
// Если в коде возникает необходимость повторить одни и те же операции по нескольку раз, функции значително упрощают этот процесс. Они представляют собой программный код, который определяется 1 раз и может вызываться на выполнение множественное количество раз.

//  Описать своими словами, зачем в функцию передавать аргумент.
// При вызове функции, когда ей передается определенное количество аргументов, они присваиваются в том порядке, в котором они указаны в функции. Когда параметр функции в скобках не указан, его дефолтное значение становится undefined. Такая ситуация может привести к дальнейшим проблемам в работе кода.


"use strict"

let num1 = prompt("Enter your first number", "228");
while (isNaN(num1)) {
    num1 = prompt("Enter your first number correctly", num1);
}

let num2 = prompt("Enter your second number", "69");
while (isNaN(num2)) {
    num2 = prompt("Enter your second number correctly", num2);
}

let operation = prompt("Enter a math operation: +, -, *, /","+");
while (operation !== "+" && operation !== "-" && operation !== "*" && operation !== "/") {
    operation = prompt("Please, choose from: +, -, *, /",operation);
}

function mathOperation(num1,num2) {
    if (operation === "+") {
        return +num1 + +num2;
    } else if (operation === "-") {
        return num1 - num2;
    } else if (operation === "*") {
        return num1 * num2;
    } else {
        return num1 / num2;
    }
}

alert(mathOperation(num1, num2));