"use strict"

//  Почему для работы с input не рекомендуется использовать события клавиатуры?
// Отслеживать клавиатурный ввод в поле input не надежно, тк какую проверку на введенные значения мы бы не написали, у пользователя всегда будет возможность воспользоваться кликом правой кнопкой мыши и пунктом "вставить" с любой информацией.

const buttonWrapper = [...document.getElementById("button-wrapper").children];
const buttons = document.querySelectorAll("button");

document.body.addEventListener("keyup", ev => {
    for (let button of buttons) {
        const attr = button.getAttribute("data-btn-content");

        if (ev.code.includes(attr)) {
            buttonWrapper.forEach(btn => btn.classList.remove("btn-blue"));
            button.classList.toggle("btn-blue")
        }
    }
})