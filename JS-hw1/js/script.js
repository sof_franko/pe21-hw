"use strict";

//  Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
// let - взаимозаменяемая переменная (можно перезаписать)
// const - статическая переменная (нельзя перезаписать)
// Вызвать переменные let и const вне логических скобок не удастся (если только они не объявлены до скобок)
// var - глобальная переменная, область видимости - вездесущая (можно вызвать даже вне скобок функции)

//  Почему объявлять переменную через var считается плохим тоном?
// Как и описано выше, переменную var можно вызвать даже вне рамок блока - область видимости является слишком большой. Это может привести к  проблеме в виде нежелательной перезаписи данных и другим ошибкам. let и const как раз разработаны для разрешения этой проблемы: при вызове переменной когда угодно вне логических скобок программа выдаст ошибку.




let name = prompt('What is your name?',"Vasya");
while (!isNaN(name)) {
    name = prompt('Please, write your name correctly', name);
}

let age = prompt("How old are you?","69");
age = parseInt(age);
while (isNaN(age)) {
    age = prompt('Please, write your age correctly', age);
    age = parseInt(age);
}

    if (age < 18) {
        alert("You are not allowed to visit this website");
    } else if (age >= 18 && age <= 22) {
        let ok = confirm("Are you sure you want to continue?");
        if (!ok) {
            alert("You are not allowed to visit this website");
        } else {
            alert(`Welcome, ${name}`);
        }
    } else {
        alert(`Welcome, ${name}`);
    }
