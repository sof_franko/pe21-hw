const changingThemeButton = document.getElementById("change-colors-button");
const page = document.body;
const pageButton = document.getElementById("new-button-style");

function changeBgColor() {
    return `rgb(${Math.floor(Math.random()*240)},${Math.floor(Math.random()*240)},${Math.floor(Math.random()*240)})`
}

window.addEventListener("DOMContentLoaded",function () {
    if (localStorage.getItem("pageBg") !== null) {
        page.style.backgroundColor = localStorage.getItem("pageBg");
        pageButton.style.backgroundColor = localStorage.getItem("pageButtonBg");
    }

changingThemeButton.addEventListener("click", ev => {

    if (localStorage.getItem("pageBg") === null) {
        page.style.backgroundColor = changeBgColor();
        pageButton.style.backgroundColor = changeBgColor();

    localStorage.setItem("pageBg", page.style.backgroundColor);
    localStorage.setItem("pageButtonBg", pageButton.style.backgroundColor);
    } else {
        page.style.backgroundColor = null;
        pageButton.style.backgroundColor = null;

        localStorage.clear();
    }

    // if (localStorage.getItem("pageBg") !== null) {
    //     changingThemeButton.onclick = function () {
    //         page.style.backgroundColor = null;
    //         pageButton.style.backgroundColor = null;
    //     }
    // }

})

})


