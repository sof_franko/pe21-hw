"use strict"

//  Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
// Экранирование - свойство отображения текстовых символов для грамотного восприятия их в коде (программе) и осуществления вывода их пользователю с помощью специальных символов, предусмотренный синтаксисом самой программы. Без этого свойства возникнут трудности в работе кода так как программа не сможет распознать отличие между синтаксисом и просто текстовыми вставками.

function createNewUser() {
    let userName = prompt("Enter your name!", "Ivan");
    while (!isNaN(userName)) {
        userName = prompt("Enter your name correctly!", userName);
    }

    let userSurname = prompt("Enter your surname!", "Pupkin");
    while (!isNaN(userSurname)) {
        userName = prompt("Enter your surname correctly!", userSurname);
    }

    let userBirthday = prompt("Enter your date of birth!", "01.01.1970");

    userBirthday = userBirthday.split(".");

    let newUser = {
        firstName: userName,
        lastName: userSurname,
        userBirthdayDate: userBirthday[0],
        userBirthdayMonth: userBirthday[1],
        userBirthdayYear: userBirthday[2],
        getLogin: function () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        },
        getAge: function () {
            const thisMoment = new Date();
            const thisYear = thisMoment.getFullYear();
            const thisMonth = thisMoment.getMonth();
            const thisDate = thisMoment.getDate();

            let userAge = thisYear - this.userBirthdayYear;
            if (this.userBirthdayMonth > thisMonth + 1) {
                userAge = userAge - 1;
            } else if (this.userBirthdayMonth === thisMonth + 1 || this.userBirthdayDate > thisDate) {
                userAge = userAge - 1;
            }
            return userAge;
        },
        getPassword: function () {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.userBirthdayYear;
        }
    };

    console.log("Your age -> " + newUser.getAge());
    console.log("Your password -> " + newUser.getPassword());

    return newUser.getLogin();
}

console.log("Your login -> " + createNewUser());
