"use strict"

//  Опишите своими словами как работает цикл forEach
//  Перебор forEach(function()) в переводе "для каждого" буквально запускает функцию в скобках для каждого элемента в массиве


function filterBy(arrayData, typeOfData) {

    return arrayData.filter(item => {
        if (typeOfData === "null") {
            if (typeof (item) !== typeOfData) {
                return true
            } else if (item == null) {
                return false
            }
        } else {
            if (item == null && typeof (item) === "object") {
                return true
            } else if (typeof (item) !== typeOfData) {
                return true
            }
        }
    })
}

console.log(filterBy([228,"merry christmas",false, ["auf",null],null,undefined,{name: "Zina"}], "object"));