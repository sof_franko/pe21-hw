"use strict"

//  Опишите своими словами, что такое метод обьекта
// Когда значением свойства может быть функция, такое свойство называют методом объекта. Чтобы вызвать свойство объекта из его метода необходимо написать this, чем и воспользуюсь при выполнении этого домашнего задания

function createNewUser() {
    let userName = prompt("Enter your name!", "Ivan");
    while (!isNaN(userName)) {
        userName = prompt("Enter your name correctly!", userName);
    }

    let userSurname = prompt("Enter your surname!", "Pupkin");
    while (!isNaN(userSurname)) {
        userName = prompt("Enter your surname correctly!", userSurname);
    }

    let newUser = {
        firstName: userName,
        lastName: userSurname,
        getLogin: function () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
        }
    };

    return newUser.getLogin();
}

console.log("Your login -> " + createNewUser());