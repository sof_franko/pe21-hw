"use strict"

//  Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
// DOM - объектная модел документа, которая представляет все содержимое страницы как объекты, с которыми можно работать (менять, добавлять, дополнять и тд)

function showList(userArray) {
    const newList = document.createElement("ul");

    userArray.map(city => {

        if (typeof city !== "object") {
            newList.insertAdjacentHTML("beforeend",`<li class = "change-font-size">${city}</li>`);
        } else {
            newList.insertAdjacentHTML("beforeend", `<li class = "change-font-size"><ul>${city.map(village => `<li>${village}</li>`).join("")}</ul></li>`)
        }
    });
    return newList
}


setTimeout(function () {
    document.write("Time is up :3")
}, 10000);


document.body.append(showList(["Antalya", "Kemer", ["Kirish", "Eskikoy"], "Side", "Istanbul", "Bodrum", "Alanya", "Manavgat"]));





















