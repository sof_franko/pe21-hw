"use strict"

//  Опишите своими словами, как Вы понимаете, что такое обработчик событий.
// Событие само по себе - это своего рода сигнал браузера о произошедшем на странице(клик, наведение, ввод). Обработчик, который можно "прикрепить" к событию - это функция (чаще всего callback) с инструкцией действий, которые нужно совершить, если это событие выполняется


/* План
1. Создать input, placeholder - "Price"
2. Проверка на ввод только числовых значений > 0
3. При нажатии на инпут сделать рамку зеленой, если при расфокусе строка пустая - снять
4. При расфокусе если число прошло проверку закрасить все поле в зеленый, создать над ним span "Текущая цена: ${введенное число}" и button "Х" привязанную к нему
6. При нажатии на button удалить её же и span, обнулить input (убрать зеленый фон и рамку)
7. Если введенное число < 0, создать под полем span "Please enter correct price", сделать рамку красной
8. При попытке исправить некорректно введенное число убрать нижний span
 */

const input = document.getElementById("input");
const warningSpan = document.getElementById("span-incorrect-price");
const priceSpan = document.getElementById("span-current-price");
const clearButton = document.createElement("button");

input.className = "input-text";
warningSpan.className = "warning-text";
priceSpan.className = "current-price";
clearButton.className = "clear-button";

clearButton.textContent = "X";

clearButton.addEventListener("click", clearValueButton)
function clearValueButton(button) {
    priceSpan.hidden = true;
    input.style.cssText = "background-color: inherit";
    input.value = "";
}

input.onblur = function () {
    if (input.value > 0) {
        warningSpan.hidden = true;
        priceSpan.hidden = false;
        priceSpan.innerText = `Текущая цена: ${input.value} тугриков`;
        priceSpan.append(clearButton);
        input.style.cssText = "background-color: greenyellow";
    } else if (input.value === "") {
        input.classList.remove("outline: 2px solid green");
    }
    else {
        priceSpan.hidden = true;
        warningSpan.hidden = false;
        warningSpan.innerText = "Please enter correct price";
        input.style.cssText = "border: 2px solid red";
    }
}

input.onfocus = function() {
    if (warningSpan.hidden === false) {
        warningSpan.hidden = true;
    }
};



